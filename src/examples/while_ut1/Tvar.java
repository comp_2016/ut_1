package examples.while_ut1;

import examples.while_ut1.ast.VarType;

public class Tvar {
	@Override
	public String toString() {
		return "Tvar [type=" + type + ", def=" + def + "]";
	}

	private final VarType type;
	private final Boolean def;
	
	public Tvar(VarType type, Boolean def) {
		super();
		this.type = type;
		this.def = def;
	}

	public VarType getType() {
		return type;
	}

	public Boolean getDef() {
		return def;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((def == null) ? 0 : def.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tvar other = (Tvar) obj;
		if (def == null) {
			if (other.def != null)
				return false;
		} else if (!def.equals(other.def))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

	
	
	
}
