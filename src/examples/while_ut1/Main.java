package examples.while_ut1;

import java.io.*;
import examples.while_ut1.ast.*;
import examples.while_ut1.parser.*;

public class Main {
	public static void main(String[] args) throws Exception {
		State state = new State();
		CheckState checkMap = new CheckState();
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			System.out.print("> ");
			for (String line; (line = in.readLine()) != null ;) {
				line = line.trim();
				try {
					if (line.length() > 0) {
						Stmt prog = (Stmt)(Parser.parse(line).value);
						state = prog.evaluate(state);
						checkMap = prog.check(checkMap);
						System.out.println(state.getStateMap().values().toString());
						System.out.println("VARIABLES "+checkMap.getCheckMap().keySet().toString());
						System.out.println("TIPOS "+checkMap.getCheckMap().values().toString());
					}
				} catch (Exception err) {
					System.err.print(err);
					err.printStackTrace();
				}
			}
	}
}