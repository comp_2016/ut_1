package examples.while_ut1.ast;

import examples.while_ut1.CheckState;
import examples.while_ut1.State;

public class Declaration extends Stmt {
	public final VarType varType;
	public final String id;

	public Declaration(VarType varType, String id) {
		super();
		this.varType = varType;
		this.id = id;
	}
	
	public VarType getVarType() {
		return varType;
	}

	@Override
	public String unparse() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public State evaluate(State state) {
		state.getStateMap().put(id, this.varType);
		return state;
	}

	@Override
	public CheckState check(CheckState s) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return false;
	}

}
