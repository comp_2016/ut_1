package examples.while_ut1.ast;

public enum VarType {
	BOOL,
	INT,
	NUM,
	STR
}
