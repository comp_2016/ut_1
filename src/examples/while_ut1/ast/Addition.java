package examples.while_ut1.ast;

import java.util.*;

import examples.while_ut1.CheckState;
import examples.while_ut1.State;

/** Representación de sumas.
*/
public class Addition extends Exp {
	public final Exp left;
	public final Exp right;

	public Addition(Exp left, Exp right) {
		this.left = left;
		this.right = right;
	}

	@Override public String unparse() {
		return "("+ left.unparse() +" + "+ right.unparse() +")";
	}

	@Override public String toString() {
		return "Addition("+ left +", "+ right +")";
	}

	@Override public int hashCode() {
		int result = 1;
		result = result * 31 + (this.left == null ? 0 : this.left.hashCode());
		result = result * 31 + (this.right == null ? 0 : this.right.hashCode());
		return result;
	}

	@Override public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null || getClass() != obj.getClass()) return false;
		Addition other = (Addition)obj;
		return (this.left == null ? other.left == null : this.left.equals(other.left))
			&& (this.right == null ? other.right == null : this.right.equals(other.right));
	}

	public static Addition generate(Random random, int min, int max) {
		Exp left; Exp right; 
		left = Exp.generate(random, min-1, max-1);
		right = Exp.generate(random, min-1, max-1);
		return new Addition(left, right);
	}

	@Override
	public Object evaluate(State state) {
		
		if(this.left.evaluate(state) instanceof Double && this.right.evaluate(state) instanceof Double) {
			return (Double)this.left.evaluate(state) + (Double)this.right.evaluate(state);
		} else {
			throw new Error("Los tipos no son compatibles para la suma.");
		}
	}

	@Override
	public VarType check(CheckState s) {
		if ((this.left.check(s) == Numeral.getType() && this.right.check(s) == Numeral.getType())){
			 return Numeral.getType(); 
		}
			 else {
					throw new Error("Los tipos no son compatibles para la suma.");
				}
		
	}
}
