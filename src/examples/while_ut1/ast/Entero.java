package examples.while_ut1.ast;

import java.util.Random;

import examples.while_ut1.CheckState;
import examples.while_ut1.State;

public class Entero extends Exp {
	public final static VarType type = VarType.INT;
	public final Integer number;

	public Entero(Integer number) {
		this.number = number;
	}
	
	public static VarType getType() {
		return type;
	}

	@Override public String unparse() {
		return number.toString();
	}

	@Override public String toString() {
		return "Entero("+ number +")";
	}

	@Override public int hashCode() {
		int result = 1;
		result = result * 31 + (this.number == null ? 0 : this.number.hashCode());
		return result;
	}

	@Override public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null || getClass() != obj.getClass()) return false;
		Numeral other = (Numeral)obj;
		return (this.number == null ? other.number == null : this.number.equals(other.number));
	}

	public static Numeral generate(Random random, int min, int max) {
		Double number; 
		number = Math.round(random.nextDouble() * 1000) / 100.0;
		return new Numeral(number);
	}

	@Override
	public Object evaluate(State state) {
		return this.number;
	}

	@Override
	public VarType check(CheckState s) {
		return this.type;
	}
}
