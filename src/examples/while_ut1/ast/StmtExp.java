package examples.while_ut1.ast;

import examples.while_ut1.CheckState;
import examples.while_ut1.State;

public class StmtExp extends Exp{
	
	public final Assignment assignment;

	public StmtExp(Assignment assign) {
		this.assignment = assign;
	}

	@Override
	public String unparse() {
		return assignment.unparse();
	}

	@Override
	public String toString() {
		return assignment.unparse();
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Object evaluate(State state) {
		// TODO Auto-generated method stub
		return this.assignment.evaluate(state);
	}

	@Override
	public VarType check(CheckState s) {
		// TODO Auto-generated method stub
		CheckState saux = this.assignment.check(s);
		
		return saux.getCheckMap().get(this.assignment.id).getType();
	}

}
