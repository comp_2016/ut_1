package examples.while_ut1.ast;

import java.util.*;
import java.util.Map.Entry;

import examples.while_ut1.CheckState;
import examples.while_ut1.State;
import examples.while_ut1.Tvar;

/** Representación de las sentencias condicionales.
*/
public class IfThenElse extends Stmt {
	public final Exp condition;
	public final Stmt thenBody;
	public final Stmt elseBody;

	public IfThenElse(Exp condition, Stmt thenBody, Stmt elseBody) {
		this.condition = condition;
		this.thenBody = thenBody;
		this.elseBody = elseBody;
	}
	
	public IfThenElse(Exp condition, Stmt thenBody) {
		this.condition = condition;
		this.thenBody = thenBody;
		this.elseBody = null;
	}

	@Override public String unparse() {
		return "if "+ condition.unparse() +" then { "+ thenBody.unparse() +" } else { "+ elseBody.unparse() +" }";
	}

	@Override public String toString() {
		return "IfThenElse("+ condition +", "+ thenBody +", "+ elseBody +")";
	}

	@Override public int hashCode() {
		int result = 1;
		result = result * 31 + (this.condition == null ? 0 : this.condition.hashCode());
		result = result * 31 + (this.thenBody == null ? 0 : this.thenBody.hashCode());
		result = result * 31 + (this.elseBody == null ? 0 : this.elseBody.hashCode());
		return result;
	}

	@Override public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null || getClass() != obj.getClass()) return false;
		IfThenElse other = (IfThenElse)obj;
		return (this.condition == null ? other.condition == null : this.condition.equals(other.condition))
			&& (this.thenBody == null ? other.thenBody == null : this.thenBody.equals(other.thenBody))
			&& (this.elseBody == null ? other.elseBody == null : this.elseBody.equals(other.elseBody));
	}

	public static IfThenElse generate(Random random, int min, int max) {
		Exp condition; Stmt thenBody; Stmt elseBody; 
		condition = Exp.generate(random, min-1, max-1);
		thenBody = Stmt.generate(random, min-1, max-1);
		elseBody = Stmt.generate(random, min-1, max-1);
		return new IfThenElse(condition, thenBody, elseBody);
	}

	@Override
	public State evaluate(State state) {
		if ((Boolean)this.condition.evaluate(state) == true){
			return this.thenBody.evaluate(state);
		} else {
			return this.elseBody.evaluate(state);
		}
	}

	@Override
	public CheckState check(CheckState s) {
			CheckState s1 = new CheckState();
			s1.getCheckMap().putAll(s.getCheckMap());
			CheckState s2 = new CheckState();
			s2.getCheckMap().putAll(s.getCheckMap());
			
			CheckState sThen = this.thenBody.check(s1);
			CheckState sElse = this.elseBody.check(s2);
			
			Set<Entry<String, Tvar>> setThen = sThen.getCheckMap().entrySet();
			Set<Entry<String, Tvar>> setElse = sElse.getCheckMap().entrySet();
			
			
			HashMap<String , Tvar> intersectionMap = new HashMap<String , Tvar>();
			for (Entry<String, Tvar> itemThen : setThen){
				System.out.println("itemThen "+itemThen.getKey()+" "+itemThen.getValue().toString());
				for (Entry<String, Tvar> itemElse : setElse){
					System.out.println("itemElse "+itemElse.getKey()+" "+itemElse.getValue().toString());
					System.out.println("itemThen.getKey() == itemElse.getKey() "+(itemThen.getKey() == itemElse.getKey()));
					System.out.println("itemThen.getValue().getType() == itemElse.getValue().getType() "+(itemThen.getValue().getType() == itemElse.getValue().getType()));
					if (itemThen.getKey().equals(itemElse.getKey()) && itemThen.getValue().getType() == itemElse.getValue().getType()){
						intersectionMap.put(itemThen.getKey(), itemThen.getValue());
					}
				}
				
			}
			System.out.println("IntersectionMap "+intersectionMap.values().toString());
			CheckState result = new CheckState();
			result.getCheckMap().putAll(intersectionMap);
			return result;
	}
}
