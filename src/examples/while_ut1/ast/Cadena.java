package examples.while_ut1.ast;

import examples.while_ut1.CheckState;
import examples.while_ut1.State;

public class Cadena extends Exp {
	public final static VarType type = VarType.STR;
	public final String cadena;
	

	public Cadena(String cadena) {
		this.cadena = cadena;
	}
	
	public static VarType getType() {
		return type;
	}

	@Override
	public String unparse() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public Object evaluate(State state) {
		return this.cadena;
	}


	@Override
	public VarType check(CheckState s) {
		return Cadena.type;
	}
}
