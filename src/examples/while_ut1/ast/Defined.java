package examples.while_ut1.ast;

import examples.while_ut1.CheckState;
import examples.while_ut1.State;

/** Representación de la sentencia que chequea si una variable
 * 	esta definida.
*/
public class Defined extends Exp {
	public final String id;

	public Defined(String id) {
		this.id = id;
	}
	
	@Override
	public String unparse() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Object evaluate(State state) {
		return state.getStateMap().containsKey(this.id);
	}

	@Override
	public VarType check(CheckState s) {
		return VarType.BOOL;
	}

}
