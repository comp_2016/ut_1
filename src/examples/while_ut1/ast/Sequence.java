package examples.while_ut1.ast;

import java.util.*;

import examples.while_ut1.CheckState;
import examples.while_ut1.State;

/** Representación de las secuencias de sentencias.
*/
public class Sequence extends Stmt {
	public final Stmt[] statements;

	public Sequence(Stmt[] statements) {
		this.statements = statements;
	}

	@Override public String unparse() {
		String result = "{ ";
					for (Stmt statement : statements) {
						result += statement.unparse() +" ";
					}
					return result +"}";
	}

	@Override public String toString() {
		return "Sequence("+ Arrays.toString(statements) +")";
	}

	@Override public int hashCode() {
		int result = 1;
		result = result * 31 + Arrays.hashCode(this.statements);
		return result;
	}

	@Override public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null || getClass() != obj.getClass()) return false;
		Sequence other = (Sequence)obj;
		return Arrays.equals(this.statements, other.statements);
	}

	public static Sequence generate(Random random, int min, int max) {
		Stmt[] statements; 
		statements = new Stmt[random.nextInt(Math.max(0, max)+1)];
		for (int i = 0; i < statements.length; i++) {
			statements[i] = Stmt.generate(random, min-1, max-1);
		}
		return new Sequence(statements);
	}

	@Override
	public State evaluate(State state) {
		State s = statements[0].evaluate(state);
		for(int i = 1; i < statements.length; i++) {
			s = statements[i].evaluate(s);
		}
		return s;
	}

	@Override
	public CheckState check(CheckState s) {
		// TODO Auto-generated method stub
		CheckState saux =  s;
		for (Stmt stament : this.statements ){
			saux = stament.check(saux);
			
		}
		return saux;
	}
}
