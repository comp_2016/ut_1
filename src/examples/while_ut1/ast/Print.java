package examples.while_ut1.ast;

import examples.while_ut1.CheckState;
import examples.while_ut1.State;

public class Print extends Stmt{

	public final Exp exp;
	
	public Print(Exp exp) {
		this.exp = exp;
	}
	
	@Override
	public String unparse() {
		return "("+ exp.unparse() +")";
	}
	
	@Override
	public String toString() {
		return "Print("+ exp +")";
	}

	@Override
	public State evaluate(State state)  {

			System.out.println(this.exp.evaluate(state));
			return state;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public CheckState check(CheckState s) {
		// TODO Auto-generated method stub
		return s;
	}

}
