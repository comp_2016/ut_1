package examples.while_ut1.ast;

import java.util.*;

import examples.while_ut1.CheckState;
import examples.while_ut1.State;

/** Representación de divisiones.
*/
public class Division extends Exp {
	public final Exp left;
	public final Exp right;

	public Division(Exp left, Exp right) {
		this.left = left;
		this.right = right;
	}

	@Override public String unparse() {
		return "("+ left.unparse() +" / "+ right.unparse() +")";
	}

	@Override public String toString() {
		return "Division("+ left +", "+ right +")";
	}

	@Override public int hashCode() {
		int result = 1;
		result = result * 31 + (this.left == null ? 0 : this.left.hashCode());
		result = result * 31 + (this.right == null ? 0 : this.right.hashCode());
		return result;
	}

	@Override public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null || getClass() != obj.getClass()) return false;
		Division other = (Division)obj;
		return (this.left == null ? other.left == null : this.left.equals(other.left))
			&& (this.right == null ? other.right == null : this.right.equals(other.right));
	}

	public static Division generate(Random random, int min, int max) {
		Exp left; Exp right; 
		left = Exp.generate(random, min-1, max-1);
		right = Exp.generate(random, min-1, max-1);
		return new Division(left, right);
	}
	
	@Override
	public Object evaluate(State state) {
		
		if(this.left.evaluate(state) instanceof Double && this.right.evaluate(state) instanceof Double) {
			if((Double)this.right.evaluate(state) == 0){
				throw new Error("No se puede dividir entre cero.");
			}
			else
			{
				return (Double)this.left.evaluate(state)/(Double)this.right.evaluate(state);			
			}
		} else {
			throw new Error("Los tipos no son compatibles para la efectuar la division.");
		}
	}

	@Override
	public VarType check(CheckState s) {
		if ((this.left.check(s) == Numeral.getType() && this.left.check(s) == Numeral.getType())){
			 return Numeral.getType(); 
		}
			 else {
					throw new Error("Los tipos no son compatibles para la division.");
				}
	}
}
