package examples.while_ut1.ast;

import examples.while_ut1.CheckState;
import examples.while_ut1.State;

public class Length extends Exp {
	public Exp expression;
	
	public Length (Exp expression){
		this.expression = expression;
	}
	@Override
	public String unparse() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Object evaluate(State state) {
		// TODO Auto-generated method stub
			String str = (String)this.expression.evaluate(state);
			if ( str instanceof String)
				return str.length();
			else throw new Error("El tipo no es compatible para el Length."); 
			
		}
	@Override
	public VarType check(CheckState s) {
		return VarType.INT;
	};
	}

	
	
