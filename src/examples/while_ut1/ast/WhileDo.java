package examples.while_ut1.ast;

import java.util.*;
import java.util.Map.Entry;

import examples.while_ut1.CheckState;
import examples.while_ut1.State;
import examples.while_ut1.Tvar;

/** Representación de las iteraciones while-do.
*/
public class WhileDo extends Stmt {
	public final Exp condition;
	public final Stmt body;

	public WhileDo(Exp condition, Stmt body) {
		this.condition = condition;
		this.body = body;
	}

	@Override public String unparse() {
		return "while "+ condition.unparse() +" do { "+ body.unparse() +" }";
	}

	@Override public String toString() {
		return "WhileDo("+ condition +", "+ body +")";
	}

	@Override public int hashCode() {
		int result = 1;
		result = result * 31 + (this.condition == null ? 0 : this.condition.hashCode());
		result = result * 31 + (this.body == null ? 0 : this.body.hashCode());
		return result;
	}

	@Override public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null || getClass() != obj.getClass()) return false;
		WhileDo other = (WhileDo)obj;
		return (this.condition == null ? other.condition == null : this.condition.equals(other.condition))
			&& (this.body == null ? other.body == null : this.body.equals(other.body));
	}

	public static WhileDo generate(Random random, int min, int max) {
		Exp condition; Stmt body; 
		condition = Exp.generate(random, min-1, max-1);
		body = Stmt.generate(random, min-1, max-1);
		return new WhileDo(condition, body);
	}

	@Override
	public State evaluate(State state) {
		State s = state;
		while((Boolean)this.condition.evaluate(state)==true)
		{
			s=body.evaluate(s);
			
		}
		return s;
	}

	@Override
	public CheckState check(CheckState s) {
		// TODO Auto-generated method stub
		CheckState s1 = new CheckState();
		s1.getCheckMap().putAll(s.getCheckMap());
		
		
		Set<Entry<String, Tvar>> antes = s1.getCheckMap().entrySet();
		CheckState conCond = null;
		// System.out.println("this.condition.check(s) "+this.condition.check(s).toString());
		if (this.condition.check(s) == VarType.BOOL){
			s = this.body.check(s);
			conCond = this.body.check(s);
		}
		Set<Entry<String, Tvar>> SetConCond = conCond.getCheckMap().entrySet();
		
		
		HashMap<String , Tvar> intersectionMap = new HashMap<String , Tvar>();
		
		for (Entry<String, Tvar> itemAntes : antes){
			 // System.out.println("itemAntes "+itemAntes.getKey()+" "+itemAntes.getValue().toString());
			for (Entry<String, Tvar> itemconCond : SetConCond){
				// System.out.println("itemconCond "+itemconCond.getKey()+" "+itemconCond.getValue().toString());
				// System.out.println("itemThen.getKey() == itemElse.getKey() "+(itemThen.getKey() == itemElse.getKey()));
				// System.out.println("itemThen.getValue().getType() == itemElse.getValue().getType() "+(itemThen.getValue().getType() == itemElse.getValue().getType()));
				if (itemAntes.getKey().equals(itemconCond.getKey()) && itemAntes.getValue().getType() == itemconCond.getValue().getType()){
					intersectionMap.put(itemconCond.getKey(), itemconCond.getValue());
				}
			}	
		}	

		CheckState result = new CheckState();
		result.getCheckMap().putAll(intersectionMap);
		return result;
	}
}
