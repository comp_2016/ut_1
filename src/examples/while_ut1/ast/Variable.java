package examples.while_ut1.ast;

import java.util.*;

import examples.while_ut1.CheckState;
import examples.while_ut1.State;

/** Representación de usos de variable en expresiones.
*/
public class Variable extends Exp {
	public final String id;

	public Variable(String id) {
		this.id = id;
	}

	@Override public String unparse() {
		return id;
	}

	@Override public String toString() {
		return "Variable("+ id +")";
	}

	@Override public int hashCode() {
		int result = 1;
		result = result * 31 + (this.id == null ? 0 : this.id.hashCode());
		return result;
	}

	@Override public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null || getClass() != obj.getClass()) return false;
		Variable other = (Variable)obj;
		return (this.id == null ? other.id == null : this.id.equals(other.id));
	}

	public static Variable generate(Random random, int min, int max) {
		String id; 
		id = ""+"abcdefghijklmnopqrstuvwxyz".charAt(random.nextInt(26));
		return new Variable(id);
	}

	@Override
	public Object evaluate(State state) {
		Object varExiste = state.getStateMap().get(this.id);
		if(varExiste !=null)
		{
			return varExiste;
		} 
		
		else {
			throw new Error("No est� definida esta variable en el diccionario");
			
		}

	}

	@Override
	public VarType check(CheckState s) {
		if (s.getCheckMap().get(this.id) != null){
			return s.getCheckMap().get(this.id).getType();
		}
		else
		{
			throw new Error("La variable "+id+" no esta definida");
		}
		
	}

}
