package examples.while_ut1.jUnit;
import examples.while_ut1.*;
import examples.while_ut1.ast.*;
import examples.while_ut1.parser.Parser;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TruthValueTest_01 {
	@Test
	public void test() {
		State state = new State();
		Stmt prog;
		try {
			prog = (Stmt)(Parser.parse("Hola = true;").value);
			state = prog.evaluate(state);
			State stateExpected = new State();
			stateExpected.getStateMap().put("Hola", true);
			assertEquals(stateExpected, state);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
						 
	}

}