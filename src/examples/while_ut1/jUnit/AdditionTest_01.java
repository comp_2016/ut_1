package examples.while_ut1.jUnit;

import examples.while_ut1.*;
import examples.while_ut1.ast.*;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class AdditionTest_01 {
	@Test
	  public void evaluatesExpression() {
		Exp left, right;
		left = new Numeral(2.31);
		right = new Numeral(1.69);
	    Addition addition = new Addition(left, right);
	    Double sum = (Double) addition.evaluate(new State());
	    assertEquals((Double) 4.00, sum);
	  }
}
