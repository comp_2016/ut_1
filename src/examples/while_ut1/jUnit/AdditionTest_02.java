package examples.while_ut1.jUnit;

import static org.junit.Assert.*;

import org.junit.Test;

import examples.while_ut1.State;
import examples.while_ut1.ast.Stmt;
import examples.while_ut1.parser.Parser;

public class AdditionTest_02 {

	@Test
	public void test() {
		State state = new State();
		Stmt prog;
		try {
			prog = (Stmt)(Parser.parse("x=20+30;").value);
			State stateExpected = new State();
			stateExpected.getStateMap().put("x", 50.0);
			state = prog.evaluate(state);
			assertEquals(stateExpected,state);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

}
