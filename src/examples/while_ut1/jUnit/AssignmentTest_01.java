package examples.while_ut1.jUnit;
import static org.junit.Assert.*;

import org.junit.Test;

import examples.while_ut1.parser.*;
import examples.while_ut1.State;
import examples.while_ut1.ast.*;

public class AssignmentTest_01 {

	@Test
	public void test() {
		State state = new State();
		Stmt prog;
		try {
			prog = (Stmt)(Parser.parse("Hola = false;").value);
			state = prog.evaluate(state);
			assertTrue(state.getStateMap().containsKey("Hola"));
			Boolean valorHola = (Boolean) state.getStateMap().get("Hola");
			assertNotNull(valorHola);
			assertFalse(valorHola);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
						 
	}

}
