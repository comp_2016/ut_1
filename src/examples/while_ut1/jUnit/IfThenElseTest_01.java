package examples.while_ut1.jUnit;

import static org.junit.Assert.*;

import org.junit.Test;

import examples.while_ut1.State;
import examples.while_ut1.ast.Stmt;
import examples.while_ut1.parser.Parser;

public class IfThenElseTest_01 {

	@Test
	public void test() {
		State state = new State();
		Stmt prog;
		try {
			prog = (Stmt)(Parser.parse("if true then x=50; else x = 0;").value);
			State stateExpected = new State();
			stateExpected.getStateMap().put("x", 50.0);
			//System.out.println(stateExpected.getStateMap().values());
			state = prog.evaluate(state);
			//System.out.println(state.getStateMap().values());
			//System.out.println(stateExpected.getStateMap().values());
			assertEquals(stateExpected,state);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
