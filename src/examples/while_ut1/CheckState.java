package examples.while_ut1;

import java.util.HashMap;
import java.util.Map;

public class CheckState {
	private final Map<String , Tvar> checkMap;

	public CheckState(Map<String, Tvar> checkMap) {
		super();
		this.checkMap = checkMap;
	}

	public Map<String, Tvar> getCheckMap() {
		return checkMap;
	}
	public CheckState(){
		
		this.checkMap = new HashMap<String,Tvar>();
	}
}
