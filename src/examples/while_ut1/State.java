package examples.while_ut1;

import java.util.HashMap;
import java.util.Map;

public class State {
	
	private final Map<String , Object> stateMap;

	public State(Map<String, Object> stateMap) {
		super();
		this.stateMap = stateMap;
	}

	public Map<String, Object> getStateMap() {
		return stateMap;
	}
	
	public State(){	
		this.stateMap = new HashMap<String,Object>();
	}
	public Boolean Igual(State s1,State s2){
		return s1.equals(s2);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((stateMap == null) ? 0 : stateMap.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		State other = (State) obj;
		if (stateMap == null) {
			if (other.stateMap != null)
				return false;
		} else if (!stateMap.equals(other.stateMap))
			return false;
		return true;
	}
	
	
	
}
